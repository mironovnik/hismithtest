"use strict";

var gulp = require("gulp");
var server = require("browser-sync").create();
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var posthtml = require("gulp-posthtml");
var autoprefixer = require("autoprefixer");
var minify = require("gulp-csso");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin-fix");
var del = require("del");
var uglify = require("gulp-uglify");
const babel = require('gulp-babel');
const svgo = require("gulp-svgo");



function clean() {
    return del("build");
}
function copy(){
    return gulp.src([
        "source/fonts/**/*.woff",
        "source/fonts/**/*.woff2",
        "source/img/**",
        "source/js/**",
        "source/*.html",
        "source/*.js"
    ], {
        base: "source"
    })
        .pipe(gulp.dest("build"));
}
function style(){
    return gulp.src("source/sass/style.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gulp.dest("build/css"))
        .pipe(minify())
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest("build/css"))
        .pipe(server.stream());

}
function compressJs(){
    return gulp.src("source/js/*.js")
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest("build/js"))
        .pipe(uglify())
        .pipe(rename("script.min.js"))
        .pipe(gulp.dest("build/js"))
        .pipe(server.stream());
}
function optimizeSvg() {
    return gulp.src("source/img/*.svg")
        .pipe(svgo())
        .pipe(gulp.dest("source/img"))
}
function html() {
    return gulp.src("source/*.html")
        .pipe(posthtml())
        .pipe(gulp.dest("build"))
        .pipe(server.stream());
}
function images(){
    return gulp.src("source/img/**/*.{png, jpg, svg}")
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.jpegtran({progressive: true}),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest("build/img"));
}
function serve(){
    server.init({
        server: "build/"
    });
    gulp.watch("source/sass/**/*.{scss, sass}", gulp.series(html,style));
    gulp.watch("source/*.html", gulp.series(html));
    gulp.watch("source/js/*.js", gulp.series(compressJs));
}


var build = gulp.series(clean, gulp.parallel( style, optimizeSvg, html, images, compressJs), copy);
gulp.task("build", build);
var liveServer = gulp.series(serve);
gulp.task("serve", liveServer);