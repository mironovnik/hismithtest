"use strict";

$(document).ready(function(){
    const bxParams = {
        controls: true,
        pagerSelector: '.main-slider__pager',
        pagerShortSeparator: '|',
        pagerType: 'short',
        auto: false,
        responsive: true,
        nextSelector: '.main-slider .arrow'
    };
    $('.main-slider__slider').bxSlider(bxParams);
});